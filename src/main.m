//
//  main.m
//  IOSTuCSoN
//
//  Created by Marco Fiorini on 17/12/14.
//  Copyright (c) 2014 Marco Fiorini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
