//
//  AppDelegate.h
//  IOSTuCSoN
//
//  Created by Marco Fiorini on 17/12/14.
//  Copyright (c) 2014 Marco Fiorini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIViewController *viewController;

@end

