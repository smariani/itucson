
package iTuCSoN;

import java.util.Date;
import org.robovm.apple.uikit.*;
import org.robovm.objc.*;
import org.robovm.objc.annotation.*;
import org.robovm.rt.bro.annotation.*;

import alice.tucson.api.EnhancedACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;

/**
 * 
 * Porting of the Command Line Interpreter. Can be booted with a TuCSoN agent ID or using a
 * default assigned one (both passed to the CLIAgent). Gets a TuCSoN ACC from
 * TucsonMetaACC then spawns the CLIAgent who manages user input.
 * 
 * @author Alessandro Ricci
 * @author (contributor) ste (mailto: s.mariani@unibo.it)
 * @author (iOS porting) Marco Fiorini (mailto: marco.fiorini2@studio.unibo.it)
 *
 */

@CustomClass("MyViewController")
public class MyViewController extends UIViewController {

    private static final int DEF_PORT = 20504;
    private static String args[] = new String[]{};
    private static IOSCLIAgent iosAgent = null;
    private Thread agentThread;
    private UITextField commandTextField;
    private UIButton sendButton;
    private static String command;
    
    public MyViewController() {
        super("MyViewController", null);
        
    }

    /**
     * 
     * View elements setters
     */
    @Property
    @TypeEncoding("v@:@")
    public void setCommandTextField(UITextField textField){
    	this.commandTextField = textField;
    }
    
    @Property
    @TypeEncoding("v@:@")
    public void setSendButton(UIButton button){
    	this.sendButton = button;
    }
    
    /**
     * 
     * Bindings to ObjC methods created with XCode, connected to the .nib file
     */
    @Callback
    @BindSelector("viewDidLoad")
    private static void viewDidLoad(MyViewController self, Selector sel){
    	self.command(args);
    	self.sendButton.setEnabled(false);
    	
    }
    
    @Callback
    @BindSelector("sendCommand:")
    private static void sendCommand(MyViewController self, Selector sel, UIButton button){
    	command = self.commandTextField.getText();
    	iosAgent.setCommand(command);
    }
     
    @Callback
    @BindSelector("commandChanged:")
    private static void commandChanged(MyViewController self, Selector sel,
                                      UITextField textField)
    {
      boolean choice = self.commandTextField.getText().isEmpty();
      self.enableSendButton(!choice);
    }
    
    /**
     *     
     * Instance methods
     */
    private void enableSendButton(boolean choice)
    {
      if ((sendButton.isEnabled() && choice == false) ||
          (!sendButton.isEnabled() && choice == true))
        sendButton.setEnabled(choice);
    }
    
    public void command(String[] args){
    	
    	log("--------------------------------------------------------------------------------");
    	log("Booting TuCSoN Command Line Intepreter...");
    	if (alice.util.Tools.isOpt(args, "-?") || alice.util.Tools.isOpt(args, "-help")) {
    		log("Arguments: {-aid <agent_identifier>} {-netid <node_address>} {-portno <portno>} {-? | -help}");
    	} else {
    		String aid = null;
    		String node = null;
    		int port;
    		if (alice.util.Tools.isOpt(args, "-aid")) {
    			aid = alice.util.Tools.getOpt(args, "-aid");
    		} else {
    			aid = "cli" + System.currentTimeMillis();
    		}
    		if (alice.util.Tools.isOpt(args, "-netid")) {
    			node = alice.util.Tools.getOpt(args, "-netid");
    		} else {
    			node = "localhost";
    		}
    		if (alice.util.Tools.isOpt(args, "-portno")) {
    			port = Integer.parseInt(alice.util.Tools.getOpt(args, "-portno"));
    		} else {
    			port = DEF_PORT;
    		}
    		log("Version "+ alice.tucson.api.TucsonMetaACC.getVersion());
    		log("--------------------------------------------------------------------------------");
    		log(new Date().toString());
    		log("Demanding for TuCSoN default ACC on port < " + port + " >...");
    		EnhancedACC context = null;
    		try {
    			context = TucsonMetaACC.getContext(new TucsonAgentId(aid), node, port);
    		} catch (final TucsonInvalidAgentIdException e) {
    			e.printStackTrace();
    			System.exit(-1);
    		}
    		if (context != null) {
    			log("Spawning CLI TuCSoN agent...");
    			log("--------------------------------------------------------------------------------");
    			init_iosCliAgent(context, node, port);
    		} else {
    			System.err.println("[CommandLineInterpreter]: Context not available now, please re-try.");
    		}
    	}
    }
    	
    private static void log(final String s) {
        System.out.println("[CommandLineInterpreter]: " + s);
    }
    
    @SuppressWarnings("static-access")
    private void init_iosCliAgent(EnhancedACC context, String node, int port){
    	
    	this.iosAgent = new IOSCLIAgent(context, node, port, this);
    	this.agentThread = new Thread(this.iosAgent);
		this.agentThread.start();
    }
}
