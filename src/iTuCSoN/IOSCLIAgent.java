package iTuCSoN;

import java.util.concurrent.Semaphore;

import alice.tucson.api.EnhancedACC;
import alice.logictuple.LogicTuple;
import alice.logictuple.LogicTupleOpManager;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tucson.parsing.TucsonOpParser;
import alice.tucson.service.TucsonCmd;
import alice.tuplecentre.api.exceptions.InvalidOperationException;
import alice.tuplecentre.api.exceptions.InvalidTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuprolog.Parser;

/**
 * Porting of the Command Line Interpreter TuCSoN agent. Waits for user input, properly parses
 * the issued command, then process it calling the corresponding method on the
 * ACC (gained through constructor). It blocks until TuCSoN reply comes.
 * 
 * @author Alessandro Ricci
 * @author (contributor) ste (mailto: s.mariani@unibo.it)
 * @author (iOS porting) Marco Fiorini (mailto: marco.fiorini2@studio.unibo.it)
 * 
 */

@SuppressWarnings("serial")
public class IOSCLIAgent extends alice.util.Automaton{

	 private final EnhancedACC context;
	 private final String node;
	 private final int port;
	 private String st = "";
	 private String command = "";
	 private MyViewController viewController;
	 private Semaphore sem;
	 

	 /**
	  * 
	  * @param ctx
	  *            the ACC held by the CLI
	  * @param n
	  *            the network address of the TuCSoN node to be considered
	  *            default
	  * @param p
	  *            the listening port of the TuCSoN node to be considered default
	  */
	
	public IOSCLIAgent(final EnhancedACC ctx, final String n, final int p, MyViewController vwCntrllr) {
	        super();
	        this.context = ctx;
	        this.node = n;
	        this.port = p;
	        this.viewController = vwCntrllr;
	        this.sem = new Semaphore(0, true);
	    }
	 
	@Override
	public void boot() {
		IOSCLIAgent.log("CLI agent listening to user...");
		this.become("goalRequest");
	}
	
	private static void busy() {
        System.out.print("[CLI]: ... ");
    }

    private static void error(final String s) {
        System.err.println("[CLI]: Unknown command <" + s + ">");
    }

    /*private static void input() {
        System.out.print("[CLI]: ?> ");
    }*/

    private static void log(final String s) {
        System.out.println("[CLI]: " + s);
    }

    private static void prompt(final String s) {
        System.out.println(" -> " + s);
    }
    
    private void read(){
    	try {
			sem.acquire();
		} catch (InterruptedException e) {
			error("error in reading");
		}
    	this.st = this.command;
    	log("command in read: "+command);
    	log("ST in read: "+st);
    }
    
    public void setCommand(String newCommand){
    	
    	this.command = newCommand;
    	
    	log("set Command: "+this.command);
    	sem.release();
    }
    
    
    
    /**
     * Main reasoning cycle
     */
	public void goalRequest(){
		
		System.out.println("[CLI]: " + "| ACC context "+context + " | Node " + node + " | Port " + port);
		this.st = "";
		this.command = "";
		while ("".equals(st)) {
            
			try{
				read();
			} catch (final Exception ex){
				System.err.println("[CLI]: " + ex);
                new IOSCLIAgent(this.context, this.node, this.port, this.viewController).boot();
			}
		}
		
		try {
            
        } catch (final Exception ex) {
            System.err.println("[CLI]: " + ex);
            new IOSCLIAgent(this.context, this.node, this.port, this.viewController).boot();
        }
		final TucsonOpParser parser = new TucsonOpParser(st, this.node, this.port);
		try {
            parser.parse();
        } catch (final TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        }
        final TucsonCmd cmd = parser.getCmd();
        
        final TucsonTupleCentreId tid = parser.getTid();
        
        if (cmd == null || tid == null) {
            IOSCLIAgent.error(st);
        } else if ("quit".equals(cmd.getPrimitive())
                || "exit".equals(cmd.getPrimitive())) {
            try {
                IOSCLIAgent.log("Releasing ACC held (if any)...");
                this.context.exit();
            } catch (final TucsonOperationNotPossibleException ex) {
                System.err.println("[CLI]: " + ex);
            } finally {
                IOSCLIAgent.log("I'm done, have a nice day :)");
                this.become("end");
            }
        } else {
        	/**
             * Tokenize TuCSoN primitive & involved argument
             */
        	final String methodName = cmd.getPrimitive();
        	final String tuple = cmd.getArg();
        	try{
        		/**
                 * Admissible Ordinary primitives
                 */
                // what about timeout? it returns null too...how to discriminate
                // inp/rdp failure?
                if ("spawn".equals(methodName)) {
                    final LogicTuple t = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.spawn(tid, t,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success");
                    } else {
                        IOSCLIAgent.prompt("failure");
                    }
                } else if ("out".equals(methodName)) {
                    final LogicTuple t = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.out(tid, t,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("in".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.in(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("rd".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.rd(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("inp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.inp(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("rdp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.rdp(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("no".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.no(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("nop".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.nop(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("set".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.set(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("get".equals(methodName)) {
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.get(tid,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("out_all".equals(methodName)) {
                    final LogicTuple t = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.outAll(tid, t,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("rd_all".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.rdAll(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("no_all".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.noAll(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("in_all".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.inAll(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("urd".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.urd(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("uno".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.uno(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("urdp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.urdp(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("unop".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.unop(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("uin".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.uin(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("uinp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.uinp(tid, templ,
                            (Long) null);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("out_s".equals(methodName)) {
                    final LogicTuple t = new LogicTuple(Parser.parseSingleTerm(
                            "reaction(" + tuple + ")",
                            new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.outS(tid,
                            new LogicTuple(t.getArg(0)),
                            new LogicTuple(t.getArg(1)),
                            new LogicTuple(t.getArg(2)), Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("in_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.inS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                            new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("rd_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.rdS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                            new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("inp_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.inpS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                            new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("rdp_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.rdpS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                            new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("no_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.noS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                            new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("nop_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.nopS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                            new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: " + op.getLogicTupleResult());
                    } else {
                        IOSCLIAgent.prompt("failure: " + op.getLogicTupleResult());
                    }
                } else if ("set_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm(tuple,
                                    new LogicTupleOpManager()));
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.setS(tid, templ,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("get_s".equals(methodName)) {
                    IOSCLIAgent.busy();
                    final ITucsonOperation op = this.context.getS(tid,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        IOSCLIAgent.prompt("success: "
                                + op.getLogicTupleListResult());
                    } else {
                        IOSCLIAgent.prompt("failure: "
                                + op.getLogicTupleListResult());
                    }
                } else if ("help".equals(methodName)
                        || "man".equals(methodName)
                        || "syntax".equals(methodName)) {
                    IOSCLIAgent.log("--------------------------------------------------------------------------------");
                    IOSCLIAgent.log("TuCSoN CLI Syntax:");
                    IOSCLIAgent.log("");
                    IOSCLIAgent.log("\t\ttcName@ipAddress:port ? CMD");
                    IOSCLIAgent.log("");
                    IOSCLIAgent.log("where CMD can be:");
                    IOSCLIAgent.log("");
                    IOSCLIAgent.log("\t\tout(Tuple)");
                    IOSCLIAgent.log("\t\tin(TupleTemplate)");
                    IOSCLIAgent.log("\t\trd(TupleTemplate)");
                    IOSCLIAgent.log("\t\tno(TupleTemplate)");
                    IOSCLIAgent.log("\t\tinp(TupleTemplate)");
                    IOSCLIAgent.log("\t\trdp(TupleTemplate)");
                    IOSCLIAgent.log("\t\tnop(TupleTemplate)");
                    IOSCLIAgent.log("\t\tget()");
                    IOSCLIAgent.log("\t\tset([Tuple1, ..., TupleN])");
                    IOSCLIAgent.log("\t\tspawn(exec('Path.To.Java.Class.class')) | spawn(solve('Path/To/Prolog/Theory.pl', Goal))");
                    IOSCLIAgent.log("\t\tin_all(TupleTemplate, TupleList)");
                    IOSCLIAgent.log("\t\trd_all(TupleTemplate, TupleList)");
                    IOSCLIAgent.log("\t\tno_all(TupleTemplate, TupleList)");
                    IOSCLIAgent.log("\t\tuin(TupleTemplate)");
                    IOSCLIAgent.log("\t\turd(TupleTemplate)");
                    IOSCLIAgent.log("\t\tuno(TupleTemplate)");
                    IOSCLIAgent.log("\t\tuinp(TupleTemplate)");
                    IOSCLIAgent.log("\t\turdp(TupleTemplate)");
                    IOSCLIAgent.log("\t\tunop(TupleTemplate)");
                    IOSCLIAgent.log("\t\tout_s(Event,Guard,Reaction)");
                    IOSCLIAgent.log("\t\tin_s(EventTemplate, GuardTemplate, ReactionTemplate)");
                    IOSCLIAgent.log("\t\trd_s(EventTemplate, GuardTemplate, ReactionTemplate)");
                    IOSCLIAgent.log("\t\tinp_s(EventTemplate, GuardTemplate ,ReactionTemplate)");
                    IOSCLIAgent.log("\t\trdp_s(EventTemplate, GuardTemplate, ReactionTemplate)");
                    IOSCLIAgent.log("\t\tno_s(EventTemplate, GuardTemplate, ReactionTemplate)");
                    IOSCLIAgent.log("\t\tnop_s(EventTemplate, GuardTemplate, ReactionTemplate)");
                    IOSCLIAgent.log("\t\tget_s()");
                    IOSCLIAgent.log("\t\tset_s([(Event1,Guard1,Reaction1), ..., (EventN,GuardN,ReactionN)])");
                    IOSCLIAgent.log("--------------------------------------------------------------------------------");
                } else if ("o/".equals(methodName)) {
                    IOSCLIAgent.prompt("\\o");
                } else if ("\\o".equals(methodName)) {
                    IOSCLIAgent.prompt("o/");
                } else if ("hi".equalsIgnoreCase(methodName)) {
                    IOSCLIAgent.prompt("Hi there!");
                } else {
                    IOSCLIAgent.error(methodName);
                }
            } catch (final TucsonOperationNotPossibleException e) {
                e.printStackTrace();
            } catch (final UnreachableNodeException e) {
                e.printStackTrace();
            } catch (final OperationTimeOutException e) {
                e.printStackTrace();
            } catch (final InvalidTupleException e) {
                e.printStackTrace();
            } catch (final InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        
        this.become("goalRequest");
        
	}
}
