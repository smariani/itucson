
package iTuCSoN;

import org.robovm.apple.foundation.*;
import org.robovm.apple.uikit.*;


/**
 * Main class of the IOSTuCSoN app, sets the Window and the Application options, then launches the app
 * 
 * @author Marco Fiorini (mailto: marco.fiorini2@studio.unibo.it)
 * 
 */


public class IOSTuCSoN extends UIApplicationDelegateAdapter{
	
	private UIWindow window = null;
    private MyViewController myViewController = null;

    @Override
    public boolean didFinishLaunching(UIApplication application,
    		UIApplicationLaunchOptions launchOptions) {
  
        window = new UIWindow(UIScreen.getMainScreen().getBounds());
        // This addStrongRef() call makes this MyApp instance GC reachable until the
        // ObjC UIApplication instance is deallocated (which doesn't happen). Any Java instance
        // reachable from this MyApp instance (like this.window) will never get GCed.
        application.addStrongRef(this);
        myViewController = new MyViewController();
        window.setRootViewController(myViewController);
        window.makeKeyAndVisible();
        
        return true;
    }
    
    @Override
    public void didReceiveMemoryWarning(UIApplication application) {
    	System.out.println("Memory Before system.gc "+Runtime.getRuntime().totalMemory());
        for (int i = 0; i < 10; i++) {
            System.gc();
        }
        System.out.println("Memory After system.gc "+Runtime.getRuntime().totalMemory());
    }
    
    public static void main(String[] args) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(args, null, IOSTuCSoN.class);
        pool.close();
    }
}


