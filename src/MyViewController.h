//
//  MyViewController.h
//  IOSTuCSoN
//
//  Created by Marco Fiorini on 17/12/14.
//  Copyright (c) 2014 Marco Fiorini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *commandTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

- (IBAction)sendCommand:(id)sender;
- (IBAction)commandChanged:(id)sender;


@end
